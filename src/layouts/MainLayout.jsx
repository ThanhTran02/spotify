import axios from "axios";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Outlet } from "react-router-dom";
import styled from "styled-components";

import { Sidebar } from "../components";
import { Footer } from "../components";
import { spotifyAction } from "../store/slice";

const MainLayOut = () => {
  const { token } = useSelector((state) => state.spotify);
  const dispatch = useDispatch();
  useEffect(() => {
    const getUserInfo = async () => {
      const { data } = await axios.get("https://api.spotify.com/v1/me", {
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
        },
      });
      const userInfo = {
        userId: data.id,
        userName: data.display_name,
        img: data.images[0].url,
      };
      console.log(userInfo);
      dispatch(spotifyAction.setUser(userInfo));
    };
    getUserInfo();
  }, []);
  return (
    <Container>
      <div className="spotify__body">
        <Sidebar />
        <div className="body mt-2 rounded-md">
          <Outlet />
        </div>
      </div>
      <div className="spotify__footer">
        <Footer />
      </div>
    </Container>
  );
};

export default MainLayOut;
const Container = styled.div`
  max-width: 100vw;
  max-height: 100vh;
  overflow: hidden;
  display: grid;
  grid-template-rows: 87vh 13vh;

  .spotify__body {
    display: grid;
    grid-template-columns: 23vw 77vw;
    height: 100%;
    width: 100%;
    background: black;
    .body {
      height: 100%;
      width: 100%;
      overflow: auto;

      background-color: #121212;
      &::-webkit-scrollbar {
        width: 0.7rem;
        max-height: 2rem;
        &-thumb {
          background-color: rgba(255, 255, 255, 0.6);
        }
      }
    }
  }
`;

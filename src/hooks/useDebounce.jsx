import { useState, useEffect } from 'react'
import React from 'react'

const useDebounce = (value, delay) => {
    const [defaultValue, setDefaultValue] = useState(value)
    useEffect(() => {
        const hanlder = setTimeout(() => setDefaultValue(value), delay);
        return () => clearTimeout(hanlder);
    }, [value]);
    return defaultValue;

}

export default useDebounce
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";

import { Card } from "../components";
import { Navbar } from "../components";
import { spotifyAction } from "../store/slice";

const Home = () => {
  const { token } = useSelector((state) => state.spotify);
  const [trackList, setTrackList] = useState([]);
  const [ablumList, setAblumList] = useState([]);
  const dispatch = useDispatch();
  useEffect(() => {
    const getInitialPlaylist = async () => {
      try {
        const [responseTrack, responseAblum] = await axios.all([
          axios.get(
            `https://api.spotify.com/v1/tracks?ids=7ouMYWpwJ422jRcDASZB7P%2C4VqPOruhp5EdPBeR92t6lQ%2C2takcwOaAZWiXQijPHIx7B'`,
            {
              headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json",
              },
            }
          ),
          axios.get(
            `https://api.spotify.com/v1/albums?ids=382ObEPsp2rxGrnsizN5TX%2C1A2GTWGtFfWp7KSQTwWOyo%2C2noRn2Aes5aoNVsU6iWThc`,
            {
              headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json",
              },
            }
          ),
        ]);
        const dataTrack = responseTrack.data.tracks;
        const listTracks = dataTrack
          ?.map((item) => {
            if (item) {
              return {
                id: item.album.id,
                name: item.album.name,
                artists: item.album.artists.map((artist) => artist.name),
                image: item.album.images[1].url,
                release_date: item.album.release_date,
              };
            } else {
              return null;
            }
          })
          .filter((track) => track !== null);
        setTrackList(listTracks);
        dispatch(spotifyAction.setTrackList(listTracks));
        // ablumData
        const dataAblum = responseAblum.data.albums;
        const listAblum = dataAblum?.map((item) => {
          return {
            id: item.id,
            name: item.name,
            artists: item.artists.map((artist) => artist.name),
            image: item.images[1].url,
            release_date: item.release_date,
          };
        });
        setAblumList(listAblum);
      } catch (error) {
        console.log("Error fetching API data:", error);
      }
    };
    getInitialPlaylist();
  }, [token]);
  if (!trackList || !ablumList) {
    return <div>Loading...</div>;
  }
  return (
    <Container>
      <div className="header">
        <Navbar activeNext={true} homeNavbar={true} />
      </div>
      <div className="bg-[#121212] p-10">
        <h2 className="text-white text-3xl font-bold">Album phổ biến</h2>
        <div className="grid grid-cols-4 gap-4 mt-4">
          {ablumList.map((product) => (
            <Card product={product} key={product.id} />
          ))}
          {ablumList.map((product) => (
            <Card product={product} key={product.id} />
          ))}
        </div>
      </div>
    </Container>
  );
};

export default Home;
const Container = styled.div``;

import React from "react";
import { Route, Routes } from "react-router-dom";

import Discover from "./Discover";
import Home from "./Home";
import Search from "./Search";
import MainLayout from "../layouts/MainLayout";
import { PATH } from "../config/path";
import Play from "./Play";

const Main = () => {
  const clearUrl = () => {
    const currentURL = window.location.href;
    const newURL = currentURL.split("#")[0];

    window.history.replaceState({}, document.title, newURL);
  };
  clearUrl();

  return (
    <div>
      <Routes>
        <Route element={<MainLayout />}>
          <Route index element={<Home />} />
          <Route path={PATH.discover} element={<Discover />} />
          <Route path={PATH.search} element={<Search />} />
          <Route path={PATH.playlists} element={<Play />} />
        </Route>
      </Routes>
    </div>
  );
};

export default Main;

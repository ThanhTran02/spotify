import axios from "axios";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import Card from "../components/Card";
import Navbar from "../components/Navbar";

export const Search = () => {
  const dispatch = useDispatch();
  const { token, searchList } = useSelector((state) => state.spotify);
  const [products, setProducts] = useState();
  const [albums, setAlbums] = useState([]);

  const inputSearch = true;
  var searchParameters = {
    method: "GET",
    headers: {
      Authorization: "Bearer " + token,
      "Content-Type": "application/json",
    },
  };

  useEffect(() => {
    const getListProducts = async () => {
      const response = await axios.get(
        `https://api.spotify.com/v1/search?q=remaster%2520track%3ADoxy%2520artist%3AMiles%2520Davis&type=album`,
        {
          headers: {
            Authorization: "Bearer " + token,
            "Content-Type": "application/json",
          },
        }
      );
      const data = response.data.albums.items;
      const listItems = data?.map((item) => {
        return {
          id: item.id,
          name: item.name,
          artists: item.artists.map((artist) => artist.name),
          image: item.images[1].url,
          release_date: item.release_date,
        };
      });
      setProducts(listItems);
    };
    getListProducts();
  }, [token]);
  return (
    <div>
      <Navbar inputSearch={inputSearch} />
      <div className="p-10">
        <h2 className="text-white text-2xl font-semibold">Duyệt tìm tất cả</h2>
        <div className="grid grid-cols-4 gap-4 mt-4">
          {searchList !== null
            ? searchList.map((product) => (
                <Card product={product} key={product.id} />
              ))
            : products?.map((product) => (
                <Card product={product} key={product.id} />
              ))}
        </div>
      </div>
    </div>
  );
};
export default Search;

import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";

import Main from "./pages/Main";
import Login from "./pages/Login";
import { spotifyAction } from "./store/slice";

function App() {
  const dispatch = useDispatch();
  const { token } = useSelector((state) => state.spotify);
  useEffect(() => {
    const hash = window.location.hash;
    if (hash) {
      const token = hash.substring(1).split("&")[0].split("=")[1];
      dispatch(spotifyAction.setToken(token));
    }
  }, [token]);
  return <div>{token ? <Main /> : <Login />}</div>;
}

export default App;

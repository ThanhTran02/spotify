import { combineReducers } from "@reduxjs/toolkit";
import { spotifyReducer } from "./slice";

export const rootReducer = combineReducers({
  spotify: spotifyReducer,
});

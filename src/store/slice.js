import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
  token: null,
  playlists: [],
  userInfo: null,
  selectedPlaylistId: "37i9dQZF1E37jO8SiMT0yN",
  selectedPlaylist: null,
  currentlyPlaying: null,
  playState: false,
  searchList: null,
  ListPage: null,
  trackList: null,
};

const spotifySilce = createSlice({
  name: "spotify",
  initialState,
  reducers: {
    setToken: (state, action) => {
      state.token = action.payload;
    },
    setUser: (state, action) => {
      state.userInfo = action.payload;
    },
    setSearchList: (state, action) => {
      state.searchList = action.payload;
    },
    setPlayLists: (state, action) => {
      state.playlists = action.payload;
    },
    setSelectedPlaylist: (state, action) => {
      state.selectedPlaylist = action.payload;
    },
    setTrackList: (state, action) => {
      state.trackList = action.payload;
    },
  },
});
export const { actions: spotifyAction, reducer: spotifyReducer } = spotifySilce;

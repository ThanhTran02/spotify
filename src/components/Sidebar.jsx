import React from "react";
import { styled } from "styled-components";
import { BiLibrary } from "react-icons/bi";
import { MdHomeFilled, MdSearch } from "react-icons/md";
import { IoAddOutline } from "react-icons/io5";
import { AiOutlineArrowRight } from "react-icons/ai";
import { NavLink } from "react-router-dom";

import { PATH } from "../config/path";
import Playlists from "./Playlists";
const Sidebar = () => {
  return (
    <Container>
      <div className="top__links py-[8px] px-[12px] bg-[#121212] rounded-md flex flex-col mb-0">
        <ul>
          <li className="flex items-center">
            <MdHomeFilled style={{ fontSize: "30px" }} />
            <NavLink to={PATH.home}>Trang chủ</NavLink>
          </li>
          <li className="flex items-center">
            <MdSearch style={{ fontSize: "30px" }} />
            <NavLink to={PATH.search}>Tìm kiếm</NavLink>
          </li>
        </ul>
      </div>
      <div>
        <div className="libary bg-[#121212] rounded-t-md mt-2  pl-[26px] pr-3 py-[8px] overflow-auto">
          <div className="flex justify-between items-center my-2">
            <div className="flex gap-5 hover:text-white cursor-pointer transition duration-300 ">
              <BiLibrary style={{ fontSize: "25px" }} />
              <span className="font-bold text-lg">Thư viện</span>
            </div>
            <div className="flex gap-2  ">
              <div className="  hover:text-white cursor-pointer transition duration-300">
                <IoAddOutline style={{ fontSize: "25px" }} />
              </div>
              <div className=" hover:text-white cursor-pointer transition duration-300">
                <AiOutlineArrowRight style={{ fontSize: "25px" }} />
              </div>
            </div>
          </div>
        </div>
      </div>
      <Playlists />
    </Container>
  );
};

export default Sidebar;
const Container = styled.div`
  background-color: black;
  color: #b3b3b3;
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  padding: 8px;
  .top__links {
    display: flex;
    flex-direction: column;
    }
    ul {
      list-style-type: none;
      display: flex;

      flex-direction: column;
      gap: 1rem;
      padding: 1rem;
      li{
        display: flex;
        gap: 1rem;
        cursor: poiter;
        font-size:18px;
        font-weight: bold;
        transition: 0.3s ease-in-out;
        &:hover {
          color: white;
        }
      }
    }
  }
`;

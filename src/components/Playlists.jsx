import axios from "axios";
import React, { useEffect } from "react";
import { styled } from "styled-components";
import { RiMusic2Line } from "react-icons/ri";
import { FaMagnifyingGlass } from "react-icons/fa6";
import { AiFillCaretDown } from "react-icons/ai";

import { useDispatch, useSelector } from "react-redux";
import { spotifyAction } from "../store/slice";

const Playlists = () => {
  const { token, playlists } = useSelector((state) => state.spotify);
  const dispatch = useDispatch();
  useEffect(() => {
    const getPlaylistData = async () => {
      const reponse = await axios.get(
        "https://api.spotify.com/v1/me/playlists",
        {
          headers: {
            Authorization: "Bearer " + token,
            "Content-Type": "application/json",
          },
        }
      );
      const { items } = reponse.data;
      const playlists = items.map(({ name, id }) => {
        return { name, id };
      });
      dispatch(spotifyAction.setPlayLists(playlists));
    };
    getPlaylistData();
  }, [token, dispatch]);
  const changeCurrentPlaylist = (selectedPlaylistId) => {};
  return (
    <Container className="h-full flex-col rounded-b-lg">
      <div className="flex justify-between mt-1 px-4 items-center ">
        <div className="p-2 z-10 rounded-full transition-all cursor-pointer hover:text-white duration-300 hover:bg-[#2a2a2a]">
          <FaMagnifyingGlass />
        </div>
        <p className="flex items-center transition-all cursor-pointer hover:text-white duration-300">
          Recents
          <span>
            <AiFillCaretDown />
          </span>
        </p>
      </div>
      <ul className="container-scroll px-0 ">
        {playlists.map(({ name, id }) => (
          <li
            key={id}
            onClick={() => changeCurrentPlaylist(id)}
            className="w-50  rounded-lg hover:bg-[#2a2a2a] m-0"
          >
            <div className="flex gap-5  p-2  items-center  ">
              <div className="bg-[#222222] flex flex-col justify-center items-center p-2 rounded-md h-10">
                <RiMusic2Line
                  style={{
                    fontSize: "25px",
                  }}
                />
              </div>
              <p className="font-semibold text-sm"> {name}</p>
            </div>
          </li>
        ))}
        {playlists.map(({ name, id }) => (
          <li
            key={id}
            onClick={() => changeCurrentPlaylist(id)}
            className="w-50  rounded-lg hover:bg-[#2a2a2a] m-0"
          >
            <div className="flex gap-5  p-2  items-center  ">
              <div className="bg-[#222222] flex flex-col justify-center items-center p-2 rounded-md h-10">
                <RiMusic2Line
                  style={{
                    fontSize: "25px",
                  }}
                />
              </div>
              <p className="font-semibold text-sm"> {name}</p>
            </div>
          </li>
        ))}
      </ul>
    </Container>
  );
};

export default Playlists;
const Container = styled.div`
  background: #121212;
  ul {
    list-style-type: none;
    display: flex;
    flex-direction: column;
    gap: 1rem;
    padding: 10px 0;
    height: 100%;
    max-height: 283px;
    overflow: auto;

    li {
      display: flex;
      gap: 1rem;
      cursor: pointer;
      transition: 0.3s ease-in-out;
      &:hover {
        color: white;
      }
    }
  }
`;

import axios from "axios";
import React, { useState } from "react";
import { styled } from "styled-components";
import { PiDevicesBold } from "react-icons/pi";
import { BsMenuButtonWide } from "react-icons/bs";
import { AiFillPlaySquare } from "react-icons/ai";

import { VolumeSlider } from "./";

const Volume = () => {
  // const [{ token }] = useStateProvider();
  const [clickPlay, setClickPlay] = useState(false);
  const [clickIconDevice, setClickIconDevice] = useState(false);
  const [clickIconMenu, setClickIconMenu] = useState(false);
  // const setVolume = async (e) => {
  //   await axios.put(
  //     "https://api.spotify.com/v1/me/player/volume",
  //     {},
  //     {
  //       params: {
  //         volume_percent: parseInt(e.target.value),
  //       },
  //       headers: {
  //         "Content-Type": "application/json",
  //         Authorization: "Bearer " + token,
  //       },
  //     }
  //   );
  // };
  return (
    <Container>
      <button
        onClick={() => {
          setClickPlay(!clickPlay);
          setClickIconMenu(false);
          setClickIconDevice(false);
        }}
      >
        <AiFillPlaySquare
          className={clickPlay ? "activeIcon" : ""}
          style={{
            color: "white",
            fontSize: "30px",
            paddingRight: "10px",
          }}
        />
      </button>
      <button
        onClick={() => {
          setClickPlay(false);
          setClickIconMenu(!clickIconMenu);
          setClickIconDevice(false);
        }}
      >
        <BsMenuButtonWide
          className={clickIconMenu ? "activeIcon" : ""}
          style={{
            color: "white",
            fontSize: "30px",
            paddingRight: "10px",
          }}
        />
      </button>
      <button
        onClick={() => {
          setClickPlay(false);
          setClickIconMenu(false);
          setClickIconDevice(!clickIconDevice);
        }}
      >
        <PiDevicesBold
          className={clickIconDevice ? "activeIcon" : ""}
          style={{ color: "white", fontSize: "30px", paddingRight: "10px" }}
        />
      </button>

      <VolumeSlider />
    </Container>
  );
};

export default Volume;
const Container = styled.div`
  display: flex;
  justify-content: flex-end;
  align-content: center;
  padding-right: 30px;
  padding-left: 30px;
  .activeIcon {
    fill: #1db954;
  }

  input[type="range"]::-webkit-slider-thumb {
    visibility: hidden;
  }

  input[type="range"]::-moz-range-thumb {
    visibility: hidden;
  }

  input[type="range"]::-ms-thumb {
    visibility: hidden;
  }

  input[type="range"]:hover::-webkit-slider-thumb {
    visibility: visible;
  }

  input[type="range"]:hover::-moz-range-thumb {
    visibility: visible;
  }

  input[type="range"]:hover::-ms-thumb {
    visibility: visible;
  }
`;

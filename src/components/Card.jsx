import React, { useState } from "react";
import { AiFillPlayCircle } from "react-icons/ai";
import styled from "styled-components";
const Card = ({ product }) => {
  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  return (
    <Container
      className="card-item p-5 bg-[#171717] rounded-lg flex flex-col items-start relative hover:cursor-pointer hover:bg-[#242424] duration-500 transition  h-96"
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <div className=" relative overflow-hidden ">
        <img
          src={product.image}
          alt="img"
          className="object-contain rounded-md "
        />
        <div className="absolute bottom-[10px] right-[10px]">
          <AiFillPlayCircle
            className={`my-icon ${isHovered ? "visible" : ""}`}
          />
        </div>
      </div>
      <div className=" mt-2 ">
        <h4 className="text-white font-semibold text-[18px] ">
          Name: {product.name}
        </h4>
        <div>
          {product.popularity ? (
            <h6 className="text-[#848484]">popularity: {product.popularity}</h6>
          ) : (
            ""
          )}
        </div>
        <h6 className="text-[#848484]">popularity: {product.popularity}</h6>
        <h6 className="text-[#848484] absolute bottom-9">
          Artists: {product.artists?.join(", ")}
        </h6>
        <p className="text-[#848484] absolute bottom-3">
          Release date: {product.release_date}
        </p>
      </div>
    </Container>
  );
};

export default Card;
const Container = styled.div`
  .my-icon {
    opacity: 0;
    transform: translateY(100%);
    transition: opacity 0.3s, transform 0.3s;
    fill: #1db954;
    font-size: 50px;
  }
  .my-icon.visible {
    opacity: 1;
    transform: translateY(0);
  }
`;

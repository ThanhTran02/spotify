import React from "react";
import axios from "axios";
import { styled } from "styled-components";

import {
  BsFillPlayCircleFill,
  BsFillPauseCircleFill,
  BsShuffle,
} from "react-icons/bs";
import { CgPlayTrackNext, CgPlayTrackPrev } from "react-icons/cg";
import { FiRepeat } from "react-icons/fi";
const PlayControl = () => {
  // const changeTrack = async (type) => {
  //   const response = await axios.get(
  //     `https://api.spotify.com/v1/me/player/${type}`,
  //     {},
  //     {
  //       headers: {
  //         "Content-Type": "application/json",
  //         Authorization: "Bearer " + token,
  //       },
  //     }
  //   );

  //   if (response.data !== "") {
  //     const { item } = response.data;
  //     const currentlyPlaying = {
  //       id: item.id,
  //       name: item.name,
  //       artists: item.artists.map((artist) => artist.name),
  //       image: item.album.image[2].url,
  //     };

  //     dispatch({ type: reducerCases.SET_PLAYING, currentlyPlaying });
  //   } else dispatch({ type: reducerCases.SET_PLAYING, currentlyPlaying: null });
  // };
  // const changeState = async () => {
  //   const state = playState ? "pause" : "play";
  //   const response = await axios.put(
  //     `https://api.spotify.com/v1/me/player/${state}`,
  //     {},
  //     {
  //       headers: {
  //         "Content-Type": "application/json",
  //         Authorization: "Bearer " + token,
  //       },
  //     }
  //   );
  //   dispatch({ type: reducerCases.SET_PLAYER_STATE, playState: !playState });
  // };
  return (
    <Container>
      <div className="shuffle">
        <BsShuffle />
      </div>
      <div className="previous">
        <CgPlayTrackPrev />
      </div>
      <div className="state">
        <BsFillPauseCircleFill />
        {/* : <BsFillPlayCircleFill />} */}
      </div>
      <div className="next">
        <CgPlayTrackNext />
      </div>
      <div className="repeat">
        <FiRepeat />
      </div>
    </Container>
  );
};

export default PlayControl;
const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 2rem;
  svg {
    color: #b3b3b3;
    transition: 0.2s ease-in-out;
    &:hover {
      color: white;
    }
  }
  .state {
    svg {
      color: white;
    }
  }
  .previous,
  .next,
  .state {
    font-size: 2rem;
  }
`;

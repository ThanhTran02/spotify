import React, { useEffect, useState } from "react";
import axios from "axios";

import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { spotifyAction } from "../store/slice";

import styled, { css } from "styled-components";
import { IoIosArrowForward } from "react-icons/io";
import { IoIosArrowBack } from "react-icons/io";
import { BsArrowDownCircle } from "react-icons/bs";
import { FaSearch } from "react-icons/fa";

import Tippy from "@tippyjs/react/headless";
import "tippy.js/dist/tippy.css";

import { PATH } from "../config/path";
import { useDebounce } from "../hooks";
import { Card } from "./";

const Navbar = ({
  inputSearch,
  activePre = false,
  activeNext = false,
  homeNavbar = false,
}) => {
  const dispatch = useDispatch();
  const { token, userInfo, trackList } = useSelector((state) => state.spotify);
  const [searchKey, setSearchKey] = useState("");
  const [artists, setArtists] = useState([]);
  const [dayPeriod, setDayPeriod] = useState("");

  const debounced = useDebounce(searchKey, 500);

  const navigate = useNavigate();
  const goToPreviousPage = () => {
    if (!activePre) return;
    navigate(-1);
  };

  const goToNextPage = () => {
    if (!activeNext) return;
    navigate(PATH.playlists);
  };

  const searchArtists = async (e) => {
    if (!searchKey) return;
    const { data } = await axios.get("https://api.spotify.com/v1/search", {
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      params: {
        q: searchKey,
        type: "artist",
      },
    });
    setArtists(data.artists.items);
    const searchList = artists?.map((item) => {
      return {
        id: item?.id,
        name: item?.name,
        followers: item?.followers.total,
        image: item?.images[1]?.url,
        popularity: item?.popularity,
      };
    });
    dispatch(spotifyAction.setSearchList(searchList));
  };
  useEffect(() => {
    const updateTime = () => {
      const currentTime = new Date();
      const currentHour = currentTime.getHours();

      if (currentHour >= 6 && currentHour < 12) {
        setDayPeriod("Chào buổi sáng");
      } else if (currentHour >= 12 && currentHour < 18) {
        setDayPeriod("Chào buổi chiều");
      } else {
        setDayPeriod("Chào buổi tối");
      }
    };
    updateTime();
    const timerID = setInterval(updateTime, 1000);
    searchArtists();
    return () => {
      clearInterval(timerID);
    };
  }, [debounced, token]);

  return (
    <Container isPrimary={homeNavbar} className="">
      <div className="flex justify-between items-center h-20 px-6    ">
        <div className="flex  cursor-pointer items-center ">
          <div
            className={`h-8 w-8  items-center flex justify-center rounded-full  bg-black text-white ${
              activePre ? "cursor-pointer" : "cursor-not-allowed"
            }`}
            onClick={goToPreviousPage}
          >
            <IoIosArrowBack style={{ fontSize: "22px" }} />
          </div>
          <div
            className={`h-8 w-8 items-center flex justify-center rounded-full bg-black text-white ml-2 ${
              activeNext ? "cursor-pointer" : "cursor-not-allowed"
            }`}
            onClick={goToNextPage}
          >
            <IoIosArrowForward style={{ fontSize: "22px" }} />
          </div>
          {inputSearch && (
            <div className="inputCustom flex bg-[#242424] items-center gap-2 ml-2 rounded-l-full rounded-r-full overflow-hidden px-4 py-3 w-96 hover:bg-[#343434]  focus-within:bg-[#343434] border-none outline-2 focus-within:outline outline-white  cursor-text">
              <FaSearch className="text-slate-500 " />
              <input
                type="text"
                placeholder="Bạn muốn nghe gì?"
                onChange={(e) => setSearchKey(e.target.value)}
                className="border-none bg-[#242424] focus:outline-0  w-full"
              />
            </div>
          )}
        </div>
        <div className="flex gap-3 items-center cursor-pointer">
          <div className="flex h-8 justify-between items-center text-white font-bold px-3 text-sm rounded-l-full rounded-r-full bg-black">
            <BsArrowDownCircle />
            <p className=" ml-2 hover:text-[15px] cursor-pointer ">
              Cài đặt ứng dụng
            </p>
          </div>
          <Tippy
            interactive
            delay={[300, 0]}
            render={(attrs) => (
              <div
                className="text-white bg-[#242424] text-[14px]  font-semibold rounded-md px-2 py-1 "
                tabIndex="-1"
                {...attrs}
              >
                {userInfo && <p>{userInfo.userName}</p>}
              </div>
            )}
          >
            <div className="h-8 w-8 hover:w-[33px] hover:h-[33px] rounded-full bg-black flex items-center justify-center cursor-pointer overflow-hidden p-1">
              {userInfo && (
                <img
                  src={userInfo.img}
                  alt="avatar"
                  className="object-contain rounded-full"
                />
              )}
            </div>
          </Tippy>
        </div>
      </div>
      {homeNavbar && (
        <div className="p-10">
          <h2 className="text-white text-3xl font-bold">{dayPeriod}</h2>
          <div className="grid grid-cols-4 gap-4 mt-4">
            {trackList &&
              trackList.map((product) => (
                <Card product={product} key={product.id} />
              ))}
            {trackList &&
              trackList.map((product) => (
                <Card product={product} key={product.id} />
              ))}
          </div>
        </div>
      )}
    </Container>
  );
};
export default Navbar;
const Container = styled.div`
${(props) =>
  props.isPrimary
    ? css`
        background: #093028;
        background: -webkit-linear-gradient(to top, #237a57, #093028);
        background: linear-gradient(to top, #121212, #093028);
      `
    : css`
        background-color: #121212;
      `}
  }
  .inputCustom {
    &:hover {
      background: #343434;
      input {
        background: #343434;
      }
    }
  }
  input {
    &:focus-within {
      background: #343434;
    }
  }
`;

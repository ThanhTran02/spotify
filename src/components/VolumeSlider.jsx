import React, { useState } from "react";

import { LuVolume1, LuVolumeX, LuVolume2 } from "react-icons/lu";
import { styled } from "styled-components";

const VolumeSlider = () => {
  const [volume, setVolume] = useState(0);
  const styleIcon = { color: "white", fontSize: "33px", paddingRight: "10px" };
  const handleVolumeChange = (event) => {
    const newVolume = event.target.value;
    setVolume(newVolume);
  };

  const renderVolumeIcon = () => {
    if (volume === "0") {
      return <LuVolumeX style={styleIcon} />;
    } else if (volume < 50) {
      return <LuVolume1 style={styleIcon} />;
    } else {
      return <LuVolume2 style={styleIcon} />;
    }
  };
  return (
    <Container>
      {renderVolumeIcon()}
      <div className="flex justify-center flex-col items-center">
        <input
          type="range"
          min="0"
          max="100"
          value={volume}
          className={` h-[7px] bg-gray-200 rounded-lg   accent-white  hover:accent-[#1db954] cursor-pointer transition-all `}
          onChange={(e) => handleVolumeChange(e)}
        />
      </div>
    </Container>
  );
};

export default VolumeSlider;

const Container = styled.div`
  display: flex;
  input[type="range"]::-webkit-slider-thumb {
    visibility: hidden;
  }

  input[type="range"]::-moz-range-thumb {
    visibility: hidden;
  }

  input[type="range"]::-ms-thumb {
    visibility: hidden;
  }

  input[type="range"]:hover::-webkit-slider-thumb {
    visibility: visible;
  }

  input[type="range"]:hover::-moz-range-thumb {
    visibility: visible;
  }

  input[type="range"]:hover::-ms-thumb {
    visibility: visible;
  }
`;

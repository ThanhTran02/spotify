export const PATH = {
  discover: "/discover",
  home: "/",
  search: "/search",
  playlists: "/playlists",
};
